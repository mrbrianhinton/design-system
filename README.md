# Design System

Currently separating the system into three separate "documents". Document being a loose term that is mainly used to define them as separate files.

-Application
  -Layout (tbd)
    -Container Components ie. data communication, wrap composite components (pass state data)
      -Composite Components ie. form
        -Element Componenets (pure) ie. button, icon

## Application Spec (document)
The Application Spec defines the overall goal of the specific application. An example might be something like Reminders. And in this spec you define essentially the features and goals of the application.

- Definition
- Project Goal
- Why are we building this?
- What problem are we solving?
- Who are we building it for?
- Metrics for success
- Features
  - User Scenarios   
  - User Jobs 
  - Forces
  - 
  
## Component Spec (document)
Numerous separate documents details the components that make up the application.

- Description
- Styling
- Any Interactivity
- Accessibility standards
- Reference existing components
- Link to Principle Spec

## Principles (document)
Global parameters that need to be followed when coding, and when designing.

- Global design guidelines
  - Motion
  - Whitespace recommendations
  - Font details (size, family, etc.)
  - 
  
## Roadmap
- [ ] Define basic outline
- [ ] Separate out sections into dedicated sections to detail
- [ ] 
